import com.opensymphony.xwork2.ActionSupport;

/**
 * Created by Asus on 12/6/2017.
 */
public class Product {
    private int id;
    private String name;
    private Double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String execute() {
        return "success";
    }
 }
